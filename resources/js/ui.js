function user(){
    let btnGnbUser = document.querySelector(".btn_gnb_user");
    if( btnGnbUser ){
        btnGnbUser.addEventListener("click", function(){
            this.parentNode.classList.toggle("on");
        })
    }
}

function gnbAccordion(){
    var accs = document.querySelectorAll(".accordion");
    Array.prototype.forEach.call(accs, function (acc) {
        acc.addEventListener("click", function() {
            let alreadyActive = this.classList.contains("accordion_active");

            document.querySelectorAll(".gnb_menu .panel").forEach(function(panel){
                panel.style.maxHeight = "0";
                panel.previousElementSibling.classList.remove("accordion_active");
            });
            
            if( !alreadyActive ){
                this.classList.add("accordion_active");
                if( this.nextElementSibling ){
                    var panel = this.nextElementSibling;
                    panel.style.maxHeight = panel.scrollHeight + "px";
                }
            } 
        });
    });
}

function customSelectBox(){
    var x, i, j, l, ll, selElmnt, a, b, c;
    x = document.getElementsByClassName("custom_select");
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        aSpan = document.createElement("span");
        aSpan = a.appendChild(aSpan);
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        if( a.innerHTML === "분류를 선택해주세요") {
            a.classList.add("color_lightGray");
        } else {
            a.classList.remove("color_lightGray");
        }
        b = document.createElement("ul");
        b.setAttribute("class", "select-items select-hide");
        for (j = 0; j < ll; j++) {
            c = document.createElement("li");
            c.setAttribute("class","select_items_li");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("sameSelected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        break;
                    }
                }
                h.click();
            });                    
            b.appendChild(c);
        }

        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            if( a.innerHTML === "분류를 선택해주세요") {
                a.classList.add("color_lightGray");
            } else {
                a.classList.remove("color_lightGray");
            }
            
            var branchBtn = document.getElementsByClassName("btnSelectBranch")[0];
            if( branchBtn ){
                if( a.innerHTML == "지점" ){
                    branchBtn.style.display = "block";
                } else {
                    branchBtn.style.display = "none";
                }
            }

            var directInput = document.getElementsByClassName("directly_input")[0];
            if( directInput ){
                if( a.innerHTML == "직접입력" ){
                    directInput.removeAttribute("disabled", "");
                    directInput.setAttribute("enabled", "");
                } else {
                    directInput.removeAttribute("enabled", "");
                    directInput.setAttribute("disabled", "");
                }
            }
        });
    }
    function closeAllSelect(elmnt) {
        var x, y, i, xl, yl, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        xl = x.length;
        yl = y.length;
        for (i = 0; i < yl; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < xl; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    document.addEventListener("click", closeAllSelect);
    
    var selectItemsLi = document.getElementsByClassName("select_items_li");
    var e;
    for( i = 0; i < selectItemsLi.length; i++){
        e = selectItemsLi[i];
        if (e.innerHTML == "분류를 선택해주세요"){
            selectItemsLi[i].classList.add("color_lightGray");
        }
    }

    var selectSelected;
    var branchBtn = document.getElementsByClassName("btnSelectBranch")[0];
    var noticeSelect = document.getElementsByClassName("notice_type")[0];
    if( noticeSelect ){
        selectSelected= noticeSelect.nextElementSibling.innerHTML;
        if( selectSelected == "지점" ){
            branchBtn.style.display = "block";
        } else {
            branchBtn.style.display = "none";
        }
    }

    var directInput = document.getElementsByClassName("directly_input")[0];
    var sysActionT = document.getElementsByClassName("system_action_type")[0];
    if( sysActionT ){
        selectSelected= sysActionT.nextElementSibling.innerHTML;
        if( selectSelected == "직접입력" ){
            directInput.removeAttribute("disabled", "");
            directInput.setAttribute("enabled", "");
        } else {
            directInput.removeAttribute("enabled", "");
            directInput.setAttribute("disabled", "");
        }
    }
}

function modalOpen(){
    var btnModalOpens = document.querySelectorAll(".btn_modal_open");
    btnModalOpens.forEach(function(btnModalOpen){
        var popupId = "." + btnModalOpen.getAttribute("data-popup") + ".modal_popup";
        btnModalOpen.addEventListener("click", function(){

            document.getElementsByTagName("body")[0].style.overflow = "hidden";

            var newDiv = document.createElement("div");
            newDiv.setAttribute("class", "modal_wrap");
            document.getElementById("wrap").insertBefore(newDiv, document.getElementById("wrap").children[1]);
            newDiv.appendChild(document.querySelector(popupId));
            var newDim = document.createElement("div");
            newDim.setAttribute("class", "dim");    
            let mw = document.getElementsByClassName("modal_wrap")[0];
            mw.insertBefore(newDim, mw.firstChild);

            var btnMdCloses = document.querySelectorAll(`${popupId} .btn_md_close`);
            var dim = document.getElementsByClassName("dim")[0];
            btnMdCloses.forEach(function(btnMdClose){
                btnMdClose.addEventListener("click", function(){
                    document.getElementsByTagName("body")[0].style.overflow = "";
                    dim.remove();
                    var el = getNthParent(this,4);
                    if( el.classList.contains("modal_wrap")){
                        var parent = el.parentNode;
                        while (el.firstChild) parent.insertBefore(el.firstChild, el);
                        parent.removeChild(el);
                    }
                })
            })
        });
    });
}

function noticeAlarm(){
    document.querySelector(".btn_notice").addEventListener("click", function(){
        var nw  = document.querySelector(".noti_wrap");
        if( !nw.classList.contains("on") ){
            nw.classList.add("on");
            this.classList.add("on");
            document.getElementById("contents").style.zIndex = -1;
        } else {
            nw.classList.remove("on");
            this.classList.remove("on");
            document.getElementById("contents").style.zIndex = 0;
        }
    })
    document.querySelector(".noti_bg").addEventListener("click", function(){
        document.querySelector(".noti_wrap").classList.remove("on");
        document.querySelector(".btn_notice").classList.remove("on");
        document.getElementById("contents").style.zIndex = 0;
    })
}

function selectBtn(){
    let btnSelects = document.querySelectorAll(".btn_select");
    btnSelects.forEach(function(btnSelect){
        btnSelect.addEventListener("click", function(){
            let childeNodes = this.parentNode.children;
            Array.prototype.forEach.call(childeNodes, function (childeNode) {
                childeNode.classList.remove("select");
            })
           btnSelect.classList.add("select");
        })
    })
}

function swiperSliderBtnSlect(){
    let slideSelect = document.querySelectorAll(".swiper-wrapper .swiper-slide.btn_select");
    let imgWrap = document.getElementsByClassName("img_wrap");
    slideSelect.forEach(function(ss){
        ss.addEventListener("click", function(){
            var slideSelectData = this.getAttribute("data-select");
            var select = `img_wrap_${slideSelectData}`;
            for( var i=0; i<imgWrap.length; i++){
                imgWrap[i].classList.remove("select");
            }
            document.getElementsByClassName(select)[0].classList.add("select");
        })
    });
}

function checkboxAllNoneBtn(){
    var cbAll = document.getElementById("cbAll");
    var inputCbDeletes = document.querySelectorAll("input[name='cbDelete']");

    if( cbAll ){
        cbAll.addEventListener("change", function () {
            var checked = cbAll.getAttribute("checked");
            if ( cbAll.checked == true ){
                inputCbDeletes.forEach(function(inputCbDelete){
                    inputCbDelete.checked = true;
                    cbAll.classList.remove("notAll");
                });
                document.querySelector(".btn_delete").disabled = false;
            } else {
                inputCbDeletes.forEach(function(inputCbDelete){
                    inputCbDelete.checked = false;
                    document.querySelector(".btn_delete").disabled = true;
                });
            }
        });
    }
    
    inputCbDeletes.forEach(function(inputCbDelete){
        inputCbDelete.addEventListener("change", function () {
            var inputCbDeleteLength = inputCbDeletes.length;
            var inputCheckedLength = document.querySelectorAll("input[name='cbDelete']:checked").length;
            var selectAll = (inputCbDeleteLength == inputCheckedLength);

            cbAll.checked = selectAll;
            
            if( inputCheckedLength > 0){
                document.querySelector(".btn_delete").disabled = false;
                cbAll.classList.add("notAll");
                if( selectAll ){
                    cbAll.classList.remove("notAll");
                }
            } else {
                document.querySelector(".btn_delete").disabled = true;
                cbAll.classList.remove("notAll");
            }

        });
    });
}

function flieUpLoad(){
    let uploadHiddens = document.querySelectorAll(".filebox .upload-hidden");
    uploadHiddens.forEach(function(uploadHidden){
        uploadHidden.addEventListener("change", function () {
            var uploadName = this.parentNode.querySelectorAll("input.upload-name")[0];
            var uploadNameSpan = this.parentNode.querySelectorAll("span.upload-name")[0];
            if( uploadName ){
                uploadName.value = this.files[0].name;
            }
            if( uploadNameSpan ){
                uploadNameSpan.innerText = this.files[0].name;
            }
        });
    })
}
 
function getNthParent(elem, n) {
    // find parentNode function
    return n === 0 ? elem : getNthParent(elem.parentNode, n - 1);
}

function fileTypeSelectChange(){
    let flieTypes = document.querySelectorAll(".btn_radio_txt input.file_type");
    Array.prototype.forEach.call(flieTypes, function (flieType) {
        flieType.addEventListener("change", function () {
            var parent = getNthParent(this, 4);
            var fileTypesClass = this.getAttribute("class");
            if( flieType.classList.contains("rdDirect") ){
                parent.querySelectorAll(".search_wrap")[0].classList.remove("disF");
                parent.querySelectorAll(".filebox")[0].classList.add("disF");
            } else {
                parent.querySelectorAll(".search_wrap")[0].classList.add("disF");
                parent.querySelectorAll(".filebox")[0].classList.remove("disF");
            }
        })
    })
}

function tooltipChangeModal(){
    let btnTooltipChanges= document.querySelectorAll(".btn_tooltip_change");
    Array.prototype.forEach.call(btnTooltipChanges, function (btnTooltipChange) {
        btnTooltipChange.addEventListener("click", function(){
            var popupId = "." + btnTooltipChange.getAttribute("data-popup") + ".modal_popup";
            
            document.getElementsByClassName("dim")[0].remove();
            var el = getNthParent(this,4);
            var parent = el.parentNode;
            while (el.firstChild) parent.insertBefore(el.firstChild, el);
            parent.removeChild(el);

            var newDiv = document.createElement("div");
            newDiv.setAttribute("class", "modal_wrap");
            document.getElementById("wrap").insertBefore(newDiv, document.getElementById("wrap").children[2]);
            newDiv.appendChild(document.querySelector(popupId));
            var newDim = document.createElement("div");
            newDim.setAttribute("class", "dim");
            
            let mw = document.getElementsByClassName("modal_wrap")[0];
            mw.insertBefore(newDim, mw.firstChild);
        });
    });
}

function textareaHeight(){
    var textareas = document.querySelectorAll("textarea[data-autoresize]");
    Array.prototype.forEach.call(textareas, function (textarea) {
        var offset = textarea.offsetHeight - textarea.clientHeight;
        var resizeTextarea = function(){
            textarea.style.height = "auto";
            textarea.style.height = textarea.scrollHeight + offset +"px";
        }
        textarea.addEventListener("keyup", function(){
            resizeTextarea(textarea); 
        });
        textarea.addEventListener("input", function(){
            resizeTextarea(textarea); 
        });
        resizeTextarea(textarea); 
    });
}

function tooltipTextarea(){
    let tooltipTextareas= document.querySelectorAll(".tooltipTextarea");
    Array.prototype.forEach.call(tooltipTextareas, function (tooltipTextarea) {
        tooltipTextarea.addEventListener("keydown",function(){
            var rows = tooltipTextarea.value.split('\n').length;
            var maxRows = 5;
            if( rows > maxRows){
                alert("5줄 까지만 가능합니다");
                modifiedText = tooltipTextarea.value.split("\n").slice(0, maxRows);
                tooltipTextarea.value = modifiedText.join("\n");
            }

            var maxLength = parseInt(30);
            if( this.value.length > maxLength ){
                alert("글자 수는 공백 포함 30자 이내로 입력 가능합니다.");
                this.value = this.value.substring(0,maxLength);
            }
        })
    });
}

function modalTableSelect(){
    let modalTableTrs= document.querySelectorAll(".modal_popup table tr");
    Array.prototype.forEach.call(modalTableTrs, function (modalTableTr) {
        modalTableTr.addEventListener("click", function(){
            var child = modalTableTr.parentNode.children;
            for( var i=0; i<child.length; i++){
                child[i].classList.remove("select");
            }
            modalTableTr.classList.add("select");
        });
    });
}

document.addEventListener("DOMContentLoaded", function(){
    user();     // user&logout 
    selectBtn();     // btn adding select class
    swiperSliderBtnSlect();     //  swiper btn click -> img_wrap select
    flieUpLoad();     // custom flie upload
    fileTypeSelectChange();     // filetype select change select or fileup loader
    tooltipChangeModal();      // tooltip change modal
    textareaHeight();     // textarea change height
    checkboxAllNoneBtn();     // checkbox All, deleteBtn disabled/enabled
    noticeAlarm();     //  Alarm open/close
    modalOpen();     // 모달 오픈
    gnbAccordion();     // gnb accordion
    customSelectBox();     // custom selectbox
    tooltipTextarea();     // tooltip textarea limit length/line 
    modalTableSelect();     // modalPopup table click add/remove select
});

$(function() {
    let options = {};
    if ( $(".model_slide .swiper-slide").length < 4 ) {
        options = { 
            width: '352',
            loop:false, 
            setWrapperSize: true,
            prevButton: '.swiper-button-prev',
            watchOverflow : true,
            slidesPerView: "auto",
            paginationClickable: true,
            spaceBetween: 12,
            navigation: {
                nextEl: '.btn_model_next',
            },
        }
    } else { 
        options = { 
            width: '352',
            loop:true, 
            setWrapperSize: true,
            prevButton: '.swiper-button-prev',
            watchOverflow : true,
            slidesPerView: 3,
            paginationClickable: true,
            spaceBetween: 12,
            navigation: {
                nextEl: '.btn_model_next',
            },
        } 
    } 
    var swiper = new Swiper('.model_slide_div', options);
})
